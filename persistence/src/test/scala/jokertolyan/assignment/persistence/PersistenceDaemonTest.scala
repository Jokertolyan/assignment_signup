package jokertolyan.assignment.persistence

import java.util.UUID
import java.util.concurrent.atomic.AtomicBoolean

import cakesolutions.kafka.KafkaProducer.Conf
import cakesolutions.kafka.{KafkaProducer, KafkaProducerRecord}
import com.dimafeng.testcontainers._
import com.typesafe.config.{Config, ConfigFactory, ConfigValueFactory}
import jokertolyan.assignment.common.{Credentials, CredentialsSerializer, UuidSerializer}
import org.scalatest.concurrent.Eventually
import org.scalatest.time.SpanSugar._
import org.scalatest.{MustMatchers, OptionValues, WordSpec}
import org.testcontainers.containers.Network
import scalikejdbc._

class PersistenceDaemonTest extends WordSpec with ForAllTestContainer with MustMatchers with OptionValues with Eventually {
  override implicit val patienceConfig = PatienceConfig(timeout = 5 seconds, interval = 0.5 seconds)

  lazy val network = Network.newNetwork()

  lazy val zookeeper = GenericContainer("wurstmeister/zookeeper").configure(c => {
    c.withNetwork(network)
    c.withNetworkAliases("zookeeper")
  })

  // as for me it's not ok to start test kafka always on port 9092, but it's not quite easy to configure kafka to work on any port, so I decided to omit it during this test assignment.
  lazy val kafka = FixedHostPortGenericContainer("wurstmeister/kafka",
    exposedPorts = Seq(9092), exposedHostPort = 9092, exposedContainerPort = 9092,
    env = Map("KAFKA_ADVERTISED_HOST_NAME" -> "localhost", "KAFKA_ADVERTISED_PORT" -> "9092", "KAFKA_ZOOKEEPER_CONNECT" -> "zookeeper:2181")
  ).configure(_.withNetwork(network))

  lazy val postgres = GenericContainer("postgres", Seq(5432), Map("POSTGRES_USER" -> "signup"))

  override val container: Container = MultipleContainers(zookeeper, kafka, postgres)

  lazy val config: Config = ConfigFactory.load()
    .withValue("kafka.bootstrap.servers", ConfigValueFactory.fromAnyRef(s"${kafka.containerIpAddress}:9092"))
    .withValue("db.url", ConfigValueFactory.fromAnyRef(s"jdbc:postgresql://${postgres.containerIpAddress}:${postgres.mappedPort(5432)}/signup"))

  lazy val daemonStopFlag: AtomicBoolean = PersistenceDaemon.create(config)

  lazy val db = new Db(config.getConfig("db"))

  override def afterStart(): Unit = daemonStopFlag

  "A persistence daemon" should {
    "save the record to db when there new message in Kafka topic" in {
      val kafkaProducer = KafkaProducer(Conf(
        config.getConfig("kafka"), new UuidSerializer, new CredentialsSerializer
      ))
      val uuid = UUID.randomUUID()
      val creds = Credentials("some email", "some password")
      kafkaProducer.send(KafkaProducerRecord(config.getString("kafka.topic"), uuid, creds))

      eventually {
        getCredsByUuidFromDb(uuid).value mustBe creds
      }
    }
  }

  override def beforeStop(): Unit = daemonStopFlag.set(true)

  private def getCredsByUuidFromDb(uuid: UUID): Option[Credentials] = {
    import db._

    sql"SELECT email, password FROM players WHERE id=${uuid}"
      .map(rs => Credentials(rs.string("email"), rs.string("password")))
      .first().apply()
  }
}
