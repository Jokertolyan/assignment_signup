package jokertolyan.assignment.persistence

import java.util.concurrent.atomic.AtomicBoolean

import cakesolutions.kafka.KafkaConsumer
import cakesolutions.kafka.KafkaConsumer.Conf
import com.typesafe.config.{Config, ConfigFactory}
import jokertolyan.assignment.common.{CredentialsDeserializer, UuidDeserializer}

import scala.collection.JavaConverters._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success}

object Boot extends App {
  val stopFlag = PersistenceDaemon.create(ConfigFactory.load())
  println("Persistence daemon is running")
  // stop main thread if daemon stopped
  while (!stopFlag.get) {
    Thread.`yield`()
    Thread.sleep(2000)
  }
}

object PersistenceDaemon {
  def create(config: Config): AtomicBoolean = {
    val stopFlag = new AtomicBoolean(false)
    Future {
      val kafkaConsumer = KafkaConsumer(Conf(
        config.getConfig("kafka"), new UuidDeserializer, new CredentialsDeserializer
      ))
      kafkaConsumer.subscribe(Seq(config.getString("kafka.topic")).asJava)
      val db = new Db(config.getConfig("db"))

      while (!stopFlag.get()) {
        kafkaConsumer.poll(java.time.Duration.ofSeconds(1)).asScala
          .foreach(r => db.save(r.key(), r.value())) // Db.save is non-blocking so entities will be saved concurrently
      }
      kafkaConsumer.close()
    } map { _ =>
      stopFlag.set(true) // inform parent thread that the daemon is stopped
    } onComplete {
      case Success(_) => println("Persistence daemon is stopping")
      case Failure(ex) =>
        println("Persistence daemon exited abnormally")
        ex.printStackTrace()
    }
    stopFlag
  }
}
