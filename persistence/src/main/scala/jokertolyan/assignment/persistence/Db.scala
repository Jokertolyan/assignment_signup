package jokertolyan.assignment.persistence

import java.util.UUID

import com.typesafe.config.Config
import jokertolyan.assignment.common.Credentials
import scalikejdbc._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Success, Try}

class Db(dbConfig: Config) {
  Class.forName("org.postgresql.Driver")

  ConnectionPool.singleton(dbConfig.getString("url"), dbConfig.getString("user"), dbConfig.getString("pass"))

  (0 to 20).find { _ =>
    isConnectionAvailable
  } match {
    case Some(_) => println("DB connection successfully initialized")
    case None => println("Can't connect to DB")
  }

  private def isConnectionAvailable = {
    Try(ConnectionPool.get().borrow()) match {
      case Success(connection) =>
        connection.close()
        true
      case _ =>
        println("DB is not connected yet. Let's wait.")
        Thread.sleep(2000)
        false
    }
  }

  implicit val session = AutoSession

  sql"""CREATE TABLE IF NOT EXISTS players (id UUID, email TEXT, password TEXT);""".execute.apply()

  // the insert wrapped into future to avoid avoid blocking on DB
  def save(id: UUID, credentials: Credentials): Future[Int] = {
    Future(sql"INSERT INTO players VALUES (${id}, ${credentials.email}, ${credentials.password})".update.apply())
  }
}
