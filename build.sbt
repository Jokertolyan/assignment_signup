version in Global := "jokertolyan"
scalaVersion in Global := "2.12.7"
(fork in Test) in Global := true

val akkaHttp = "com.typesafe.akka" %% "akka-http" % "10.1.5"
val akkaStream = "com.typesafe.akka" %% "akka-stream" % "2.5.17"
val kafkaClient = "net.cakesolutions" %% "scala-kafka-client" % "2.0.0"
val scalikejdbc = "org.scalikejdbc" %% "scalikejdbc" % "2.5.2"
val postgres = "org.postgresql" % "postgresql" % "42.2.5"
val scalaTest = "org.scalatest" %% "scalatest" % "3.0.5" % Test
val testContainers = "com.dimafeng" %% "testcontainers-scala" % "0.20.0" % Test
val scalajHttp = "org.scalaj" %% "scalaj-http" % "2.4.1" % Test

val common = (project in file("common"))
  .settings(
    resolvers += Resolver.bintrayRepo("cakesolutions", "maven"),
    libraryDependencies ++= Seq(kafkaClient)
  )

val signup = (project in file("signup"))
  .enablePlugins(JavaAppPackaging, DockerPlugin)
  .settings(
    libraryDependencies ++= Seq(akkaHttp, akkaStream, scalaTest, testContainers, scalajHttp)
  )
  .dependsOn(common)

val persistence = (project in file("persistence"))
  .enablePlugins(JavaAppPackaging, DockerPlugin)
  .settings(
    libraryDependencies ++= Seq(scalikejdbc, postgres, scalaTest, testContainers)
  )
  .dependsOn(common)

val functionalTest = (project in file("functionalTest"))
  .settings(
    libraryDependencies ++= Seq(scalaTest, scalajHttp, scalikejdbc % Test, postgres % Test)
  )
  .dependsOn(common)

val root = (project in file("."))
  .aggregate(signup, persistence)
  .settings(
    name := "assignment_signup"
  )
