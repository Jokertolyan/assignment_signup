# Assignment signup solution by Yevhen Kramorenko

## Project structure

Project consist of 4 modules:

1. signup - implementation and tests for signup service.
1. persistence - implementation and test for persistence service.
1. root - aggregation module for 2 modules described above.
1. functionalTest - functional test implementation. Extracted to separate module to avoid influence on services build.

## Requirements

Expected that all tools described below are in PATH

1. SBT launcher 1.x (https://www.scala-sbt.org/download.html)
1. Docker 1.6.0 or higher (https://www.docker.com/get-started)
1. Docker compose 1.10.0 or higher (https://docs.docker.com/compose/install/#prerequisites)
1. Access to the Internet to download libraries and docker images.

## How to

All commands should be executed in the project root folder, if other isn't specified

### Run services' tests
* Run signup tests - `sbt signup/test`
* Run persistence tests - `sbt persistence/test`
* Run all tests (except functional) - `sbt test`

Docker tests are blinking sometimes, so if some test has failed, please, give it one more try :).

### Build docker images
* Build signup image - `sbt signup/docker:publishLocal`
* Build persistence image - `sbt persistence/docker:publishLocal`
* Build all images - `sbt docker:publishLocal`

### Start entire system
Go to <project_root>/src/main/resources folder and execute `docker-compose up -d` for up and `docker-compose down` for down

**OR**

execute `docker-compose -f <path to project_root/src/main/resources/docker-compose.yml file> up -d` for up and 
`docker-compose -f <path to project_root/src/main/resources/docker-compose.yml file> down` for down

The signup service api will be on http://<docker_host>:18080/signup address.
Postgres is on the <docker_host>:15432 location.

### Run functional test

`sbt functionalTest/test`

Functional test expects that the signup service is on localhost:18080 and DB is on localhost:15432.
Please update `<project_root>\functionalTest\src\test\scala\FunctionalTest.scala` file to use other location.
