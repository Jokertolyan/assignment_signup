package jokertolyan.assignment.signup

import java.util.UUID

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.StatusCodes.InternalServerError
import akka.http.scaladsl.model.{ContentTypes, HttpEntity}
import akka.http.scaladsl.server.Directives._
import akka.stream.ActorMaterializer
import cakesolutions.kafka.KafkaProducer.Conf
import cakesolutions.kafka.{KafkaProducer, KafkaProducerRecord}
import com.typesafe.config.{Config, ConfigFactory}
import jokertolyan.assignment.common.{Credentials, CredentialsSerializer, UuidSerializer}

import scala.concurrent.ExecutionContext.Implicits
import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.util.{Failure, Success}

object Boot extends App {
  val config = ConfigFactory.load()
  val bindingFuture = WebServer.start(config)
  println(s"Server online at http://${config.getString("server.host")}:${config.getInt("server.port")}")
  Await.ready(bindingFuture.flatMap(_.whenTerminated)(Implicits.global), Duration.Inf)
}

object WebServer {
  def start(config: Config): Future[Http.ServerBinding] = {
    implicit val system: ActorSystem = ActorSystem("my-system")
    implicit val materializer: ActorMaterializer = ActorMaterializer()
    implicit val executionContext: ExecutionContext = system.dispatcher

    val kafkaProducer = KafkaProducer(
      Conf(config.getConfig("kafka"), new UuidSerializer, new CredentialsSerializer)
    )

    val topicName = config.getString("kafka.topic")

    def sendToPersister(creds: Credentials): Future[UUID] = {
      val uuid = UUID.randomUUID()
      val record = KafkaProducerRecord(topicName, Some(uuid), creds)
      kafkaProducer.send(record).map(_ => uuid)
    }

    val route =
      path("signup") {
        post {
          formFields('email, 'password).as(Credentials) { creds =>
            onComplete(sendToPersister(creds)) {
              case Success(newUuid) => complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, newUuid.toString))
              case Failure(ex) => complete(InternalServerError -> s"An error occurred: ${ex.getMessage}")
            }
          }
        }
      }

    val bindingFuture = Http().bindAndHandle(route, config.getString("server.host"), config.getInt("server.port"))
    bindingFuture.flatMap(_.whenTerminated).onComplete(_ => {
      kafkaProducer.close()
      system.terminate()
    })
    bindingFuture
  }
}

