package jokertolyan.assignment.signup

import java.util.UUID

import akka.http.scaladsl.Http.ServerBinding
import cakesolutions.kafka.KafkaConsumer
import cakesolutions.kafka.KafkaConsumer.Conf
import com.dimafeng.testcontainers._
import com.typesafe.config.{Config, ConfigFactory, ConfigValueFactory}
import jokertolyan.assignment.common.{Credentials, CredentialsDeserializer, UuidDeserializer}
import org.scalatest.{MustMatchers, WordSpec}
import org.testcontainers.containers.Network
import scalaj.http._

import scala.collection.JavaConverters._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.duration._

class WebServerTest extends WordSpec with ForAllTestContainer with MustMatchers {

  lazy val network = Network.newNetwork()

  lazy val zookeeper = GenericContainer("wurstmeister/zookeeper").configure(c => {
    c.withNetwork(network)
    c.withNetworkAliases("zookeeper")
  })

  // as for me it's not ok to start test kafka always on port 9092, but it's not quite easy to configure kafka to work on any port, so I decided to omit it during this test assignment.
  lazy val kafka = FixedHostPortGenericContainer("wurstmeister/kafka",
    exposedPorts = Seq(9092), exposedHostPort = 9092, exposedContainerPort = 9092,
    env = Map("KAFKA_ADVERTISED_HOST_NAME" -> "localhost", "KAFKA_ADVERTISED_PORT" -> "9092", "KAFKA_ZOOKEEPER_CONNECT" -> "zookeeper:2181")
  ).configure(_.withNetwork(network))
  override val container: Container = MultipleContainers(zookeeper, kafka)

  lazy val config: Config = ConfigFactory.load()
    .withValue("kafka.bootstrap.servers", ConfigValueFactory.fromAnyRef(s"${kafka.containerIpAddress}:9092"))
    .withValue("kafka.group.id", ConfigValueFactory.fromAnyRef("somegroup"))
    .withValue("kafka.auto.offset.reset", ConfigValueFactory.fromAnyRef("earliest"))
  lazy val serverBindings: Future[ServerBinding] = WebServer.start(config)

  override def afterStart(): Unit = serverBindings

  "A signup service" should {
    "send new message to Kafka topic when signup API is called" in {
      val creds = Credentials("email@email.com", "password")
      val resp = Http("http://localhost:8080/signup").postForm(Seq("email" -> creds.email, "password" -> creds.password)).asString
      resp.is2xx mustBe true
      val generatedUuid = UUID.fromString(resp.body)

      val kafkaConsumer = KafkaConsumer(Conf(
        config.getConfig("kafka"), new UuidDeserializer, new CredentialsDeserializer
      ))
      kafkaConsumer.subscribe(Seq(config.getString("kafka.topic")).asJava)
      val receivedRecord = kafkaConsumer.poll(java.time.Duration.ofSeconds(10)).asScala.head
      receivedRecord.key() mustBe generatedUuid
      receivedRecord.value() mustBe creds
    }
  }

  override def beforeStop(): Unit = serverBindings.foreach(_.terminate(5 seconds))
}
