import java.util.UUID

import jokertolyan.assignment.common.Credentials
import org.scalatest.concurrent.Eventually
import org.scalatest.{MustMatchers, OptionValues, WordSpec}
import scalaj.http.Http
import scalikejdbc._
import org.scalatest.time.SpanSugar._

class FunctionalTest extends WordSpec with MustMatchers with OptionValues with Eventually {
  val signupServiceHost = "localhost"
  val signupServicePort = "18080"
  val dbUrl = "jdbc:postgresql://localhost:15432/signup"
  val dbUser = "signup"
  val dbPass = "signup"

  override implicit val patienceConfig = PatienceConfig(timeout = 5 seconds, interval = 0.5 seconds)

  Class.forName("org.postgresql.Driver")

  ConnectionPool.singleton(dbUrl, dbUser, dbPass)

  implicit val session = AutoSession

  // expected that the system is already up and running (it can't be rewritten to up the environment in test if needed)
  "A signup system" should {
    "create a record in DB on signup API call" in {
      val creds = Credentials("email@email.com", "password")
      val resp = Http(s"http://$signupServiceHost:$signupServicePort/signup")
        .postForm(Seq("email" -> creds.email, "password" -> creds.password)).asString
      resp.is2xx mustBe true
      val generatedUuid = UUID.fromString(resp.body)

      def recordFromDbOpt = sql"SELECT email, password FROM players WHERE id=${generatedUuid}"
        .map(rs => Credentials(rs.string("email"), rs.string("password")))
        .first().apply()

      eventually {
        recordFromDbOpt.value mustBe creds
      }
    }
  }
}
