package jokertolyan.assignment.common

import java.io.{ByteArrayInputStream, ObjectInputStream}
import java.util
import java.util.UUID

import org.apache.kafka.common.serialization.Deserializer

import scala.reflect.ClassTag

class SerializableDeserializer[T <: java.io.Serializable: ClassTag] extends Deserializer[T] {
  override def configure(configs: util.Map[String, _], isKey: Boolean): Unit = {}

  override def deserialize(topic: String, data: Array[Byte]): T = {
    val byteStream = new ByteArrayInputStream(data)
    val objectStream = new ObjectInputStream(byteStream)
    val obj = objectStream.readObject()
    if(obj.getClass == implicitly[ClassTag[T]].runtimeClass) {
      obj.asInstanceOf[T]
    } else {
      throw new IllegalArgumentException(s"$obj is not an instance of ${implicitly[ClassTag[T]].runtimeClass}")
    }
  }

  override def close(): Unit = {}
}

class CredentialsDeserializer extends SerializableDeserializer[Credentials]

class UuidDeserializer extends SerializableDeserializer[UUID]
