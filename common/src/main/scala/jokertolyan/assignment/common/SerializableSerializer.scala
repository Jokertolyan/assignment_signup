package jokertolyan.assignment.common

import java.io.{ByteArrayOutputStream, ObjectOutputStream}
import java.util
import java.util.UUID

import org.apache.kafka.common.serialization.Serializer

class SerializableSerializer[T <: java.io.Serializable] extends Serializer[T] {
  override def configure(configs: util.Map[String, _], isKey: Boolean): Unit = {}

  override def serialize(topic: String, data: T): Array[Byte] = {
    val byteStream = new ByteArrayOutputStream()
    val objectStream = new ObjectOutputStream(byteStream)
    objectStream.writeObject(data)
    objectStream.close()
    byteStream.toByteArray
  }

  override def close(): Unit = {}
}

class CredentialsSerializer extends SerializableSerializer[Credentials]

class UuidSerializer extends SerializableSerializer[UUID]
