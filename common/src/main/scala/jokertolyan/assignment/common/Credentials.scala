package jokertolyan.assignment.common

case class Credentials(email: String, password: String)
